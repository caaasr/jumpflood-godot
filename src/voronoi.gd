extends TextureRect

#onready var parent = get_owner()
var imw = 1024
#var imh = 8

var qary: Array = [
	Vector2(-1, -1),
	Vector2(0, -1),
	Vector2(1, -1),
	Vector2(1, 0),
	Vector2(1, 1),
	Vector2(0, 1),
	Vector2(-1, 1),
	Vector2(-1, 0)
]

var seeds: Array
var buffer: Array
var dynImage
var imageTexture

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	imageTexture = ImageTexture.new()
	dynImage = Image.new()

	dynImage.create(imw, imw, false, Image.FORMAT_RGB8)
	dynImage.fill(Color(0, 0, 0))
	dynImage.lock()

	#create buffer for image
	for i in range(imw * imw):
		buffer.append(null)

	#create seeds
	#the loop
	var col
	var se = Vector2()
	for i in range(25):
		var r = (randi() % 256) / 256.0
		var g = (randi() % 256) / 256.0
		var b = (randi() % 256) / 256.0
		col = Color(r, g ,b)
		se.x = randi() % imw
		se.y = randi() % imw
		seeds.append([col, se])

	for s in seeds:
		buffer[s[1].y * imw + s[1].x] = s

	for y in range(imw):
		for x in range(imw):
			var p = buffer[y * imw + x]
			if p != null:
				dynImage.set_pixel(x, y, p[0])

	for s in seeds:
		dynImage.set_pixel(s[1].x, s[1].y, Color(0, 0, 0))

	imageTexture.create_from_image(dynImage)
	self.texture = imageTexture

	dynImage.save_png("output/frame0.png")


var run = true
var d = 2
var it = 0
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		run = true

	#jump flood here
	if run:
		it += 1
		#iter(1, buffer, imw)
		if imw/d >= 1:
			iter(imw/d, buffer, imw)
			d *= 2
		else:
			iter(1, buffer, imw)
			run = false
			print("DONE")

		#buffer to image to texture
		for y in range(imw):
			for x in range(imw):
				var p = buffer[y * imw + x]
				if p != null:
					dynImage.set_pixel(x, y, p[0])

		for s in seeds:
			dynImage.set_pixel(s[1].x, s[1].y, Color(0, 0, 0))

		imageTexture.create_from_image(dynImage)
		self.texture = imageTexture
		dynImage.save_png("output/frame" + str(it) + ".png")

#a probably not great jump flood implementation
#runs on cpu
func iter(step, buf, width):
	for y in range(0, imw):
		for x in range(0, imw):
			for qo in qary:
				var q = Vector2(x, y) + (step * qo)
				if q.x < 0 or q.x >= width or q.y < 0 or q.y >= width:
					pass
				else:
					if buf[y * width + x] == null and buf[q.y * width + q.x] != null:
						buf[y * width + x] = buf[q.y * width + q.x]

					elif buf[y * width + x] != null and buf[q.y * width + q.x] != null:
						var pdist = Vector2(x, y) - buf[y * width + x][1]
						var qdist = q - buf[q.y * width + q.x][1]

						if pdist.length_squared() > qdist.length_squared():
							buf[y * width + x] = buf[q.y * width + q.x]
